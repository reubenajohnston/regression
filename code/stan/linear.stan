//Author: reuben@reubenjohnston.com
//Description: Linear regression (Normal-Gamma prior for unknown mu and r)
//References: Murphy, 2007 (section 6)
//Comments: Do not confuse the precision of the normal prior distributions (b and d) with the precision of the observation noise, r.
//  Remember, the precision is the inverse of the variance (i.e., larger precisions are associated with smaller variance and vice versa).
functions { 
	real custnormal_lpdf(real y, real mu, real r) { 
		return (-log(2*pi())/2+log(r)/2-(r/2)*(y-mu)^2); 
	} 
} 

data {
	array[3,1] int varR; //vector of variable row sizes for each element (indices for scalars will be 1)
	array[3,1] int varC; //vector of variable column sizes for each element (indices for scalars or vectors will be 1)
	array[8,1] int datR; //vector of data row lengths for each element (indices for scalars will be 1)
	array[8,1] int datC; //vector of data column lengths for each element (indices for scalars or vectors will be 1)
	array[8,1] int datN; //vector of sizes with the number of values in each element

	array[datN[1,1],1] real x;
	array[datN[2,1],1] real y;
	
	//hyperparameters and constants need to be defined as data
	real a;
	real<lower=1e-15> b;
	real c;
	real<lower=1e-15> d;
	real<lower=1e-15> g;
	real<lower=1e-15> h;		
}

parameters {
	real beta0;
	real beta1;
	real<lower=1e-15> r;
}

model {
	beta0 ~ normal(a, sqrt(1/b));//pr(beta0)~normal(a=mean,b=precision) translated to normal(mean,stddev)	
	beta1 ~ normal(c, sqrt(1/d));//pr(beta1)~normal(c=mean,d=precision) translated to normal(mean,stddev)	
	r ~ gamma(g,h);//pr(sigma^2)~invgamma(g,1/h) --> pr(1/sigma^2)~gamma(g,h)	
	
	for (i in 1:datN[1,1]) {
		y[i,1]~custnormal(beta0+beta1*x[i,1], r);
	}
}
