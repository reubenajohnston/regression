%% Linear Model
%% Independent variables
% * $x$
%% Dependent variables
% * $y(x)$
%% Support
% * $x\in(-\infty,\infty)$
% * $y\in(-\infty,\infty)$
%% Parameters
% * $\beta_0$-y intercept
% * $\beta_1$-slope
%% Equations
% * $E[y_i|x_i]=\beta_0+\beta_1*x_i+\epsilon_i$, for i=1,2,...,n
% * $\epsilon_i \sim Normal(0,r^{-1})$
%% Subtypes
% * subtype a, $\beta_0 \sim flat()$, $\beta_1 \sim flat()$
%% Linear
% This class, mcmcBayes.linear is a class that supports analyses with linear regression 
% model
classdef linear < mcmcBayes.regression   
    properties
        
    end  
    
    methods
        function obj = linear(setSubtype)
            %LINEAR 
            % Construct an instance of this class
            if nargin > 0
                superargs{1}='mcmcBayes.linear';
                superargs{2}=setSubtype;
            elseif nargin == 0
                superargs={};
            end
            obj=obj@mcmcBayes.regression(superargs{:});
        end

        function varElementIds=getVarElementIds(obj)
            if (obj.subtype=='a')%subtype a (flat, flat, flat)
                varElementIds={'beta0',1; 'beta1',2; 'r', 3};
            elseif (obj.subtype=='b')%subtype b (normal, normal, gamma)
                varElementIds={'beta0',1; 'beta1',4; 'r', 7};
            else
                error('subtype=%s is unsupported for model type=%s',obj.subtype, obj.type);
            end
        end

        function varElementId=getPhiVarElementId(obj)
            if (obj.subtype=='a')%subtype a (flat, flat, flat)
                error('marginal loglikelhood estimation not possible with flat priors');
            elseif (obj.subtype=='b')%subtype b (normal, normal, gamma), for phi, varid=4
                varElementId=10;
            else
                error('subtype=%s is unsupported for model type=%s',obj.subtype, obj.type);
            end
        end

        function datElementIds=getDatElementIds(obj)
            datElementIds={'x',1; 'y',2};
        end
    end
end

