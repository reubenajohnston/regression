%% Regression Family of Models
% * Linear
% * Polynomial degree-2
classdef regression < mcmcBayes.model
    properties
        
    end
    
    methods
        function obj = regression(setType, setSubtype)
            %REGRESSION 
            % Construct an instance of this class
            if nargin > 0
                superargs{1}='mcmcBayes.regression';
                superargs{2}=setType;
                superargs{3}=setSubtype;
            elseif nargin == 0
                superargs={};
            end
            obj=obj@mcmcBayes.model(superargs{:});
        end

        function [retPredictions]=generatePredictions(obj, samples, predictions)
            % Currently, only supporting linear regression

            varElementIds=obj.getVarElementIds();
            datElementIds=obj.getDatElementIds();
            if (strcmp(obj.type,'mcmcBayes.linear')==true)
                datid=datElementIds{1,2};
                if ~(strcmp('x',predictions.elements(datid).name))
                    warning('linear x name not found in element(datid), datid might be incorrect');
                end
                cX=predictions.elements(datid).values.valueConstant.data;

                varid=varElementIds{1,2};
                if ~(strcmp('beta0',samples.elements(varid).name))
                    warning('linear beta0 name not found in element(varid), varid might be incorrect');
                end                
                beta0=samples.elements(varid).values.valueGenerated.data;
                varid=varElementIds{2,2};
                if ~(strcmp('beta1',samples.elements(varid).name))
                    warning('linear beta1 name not found in element(varid), varid might be incorrect');
                end                 
                beta1=samples.elements(varid).values.valueGenerated.data;
                varid=varElementIds{3,2};
                if ~(strcmp('r',samples.elements(varid).name))
                    warning('linear r name not found in element(varid), varid might be incorrect');
                end                 
                r=samples.elements(varid).values.valueGenerated.data;
            else
                error('Unsupported model type for mcmcBayes.regression');
            end

            N=size(cX,1);
            T=size(beta0,1);

            idx=0;%current index
            for idx_sample=1:T
                for idx_point=1:N
                    idx=idx+1;
                    x(idx,1)=cX(idx_point);
                    %regression model we don't need to sample from a distribution using the parameter samples; i.e., the sample is generated from the following equation
                    y(idx,1)=beta0(idx_sample)+beta1(idx_sample)*cX(idx_point)+sqrt(1/r(idx_sample));
                end
            end
            retPredictions=predictions;
            datid=datElementIds{1,2};
            %construct a generated x element
            setHValueAttributes=retPredictions.elements(datid).values.hValueAttributes;
            setType=mcmcInterface.t_valueDataType.generated;
            setTfPre=retPredictions.elements(datid).values.valueConstant.tfPre;
            setTfPost=retPredictions.elements(datid).values.valueConstant.tfPost;
            newx=mcmcInterface.value(setHValueAttributes, setType, setTfPre, setTfPost, x);
            retPredictions.elements(datid).values.valueGenerated=newx;
            %save y
            datid=datElementIds{2,2}; 
            if ~(strcmp('y',predictions.elements(datid).name))
                error('linear y name not found in element(datid), datid might be incorrect');
            end              
            retPredictions.elements(datid).values.valueGenerated.data=y;
        end

        function [retPredictions]=computePredictionStatistics(obj, predictions)        
            % Currently, only supporting linear regression

            %let x.constant=predictions.elements(1).value.values.valueConstant
            %let x.generated=predictions.elements(1).value.values.valueGenerated
            %let y.generated=predictions.elements(2).value.values.valueGenerated
            %let N=number of unique values in x.constant
            %let T=number of unique samples
            %
            %every jth sample set [beta0_j, beta1_j, and r_j] (for j=1:T) generated a corresponding chunk of values of length N in x.generated and y.generated
            % i.e., the values in jth sample set were repeatedly used in every ith value in x.constant (for i=1:N) to determine y_i=beta0_j+beta1_j*x_i+r_j
            %results from iterations were all added to x.generated and y.generated (length of x.generated and y.generated are each N*T)
            %
            %compute the prediction statistics across the samples for each unique value in x.constant
            %store the results (mean, hingelo, hingehi will be arrays of length N) as a statistics object within y.generated 

            %N=size of x.constant
            %for i=1:N
            %  extract xi, yi chunks from x.generated and y.generated
            %  yhati=predictions.computeStatistics(xi,yi)
            %  save yhati entries into y.generated.statistics

            predictionStatisticsToCapture=[mcmcInterface.t_predictionStatistic.hingelo;mcmcInterface.t_predictionStatistic.mean;mcmcInterface.t_predictionStatistic.hingehi];

            datElementIds=obj.getDatElementIds();
            datid=datElementIds{1,2};
            if ~(strcmp('x',predictions.elements(datid).name))
                warning('linear x name not found in element(datid), datid might be incorrect');
            end  
            indElement=predictions.elements(datid).values;
            datid=datElementIds{2,2};
            if ~(strcmp('y',predictions.elements(datid).name))
                warning('linear y name not found in element(datid), datid might be incorrect');
            end  
            depElement=predictions.elements(datid).values;
            
            T=size(indElement.valueGenerated.data);
            N=size(indElement.valueConstant.data,1);
  
            dimensions=depElement.hValueAttributes.dimensions;
            datatype=depElement.hValueAttributes.type;

            samplecnt=1;
            datacnt=0;
            for j=1:T
                datacnt=datacnt+1;
                if datacnt==N+1
                    datacnt=1;
                    samplecnt=samplecnt+1;
                end
                yi(samplecnt,datacnt)=depElement.valueGenerated.data(j);
            end

            for i=1:N
                %estimate the bins for the chunk
                miny=min(yi(:,i));
                maxy=max(yi(:,i));
                delta=maxy-miny;
                precision=1000;
                bins=miny:delta/precision:maxy;
                if i==1
                    statistics=mcmcInterface.predictions.computeStatistics(predictionStatisticsToCapture, bins, dimensions, datatype, yi(:,i));
                else
                    tstatistics=mcmcInterface.predictions.computeStatistics(predictionStatisticsToCapture, bins, dimensions, datatype, yi(:,i));
                    statistics.hingelo=[statistics.hingelo; tstatistics.hingelo];
                    statistics.mean=[statistics.mean; tstatistics.mean];
                    statistics.hingehi=[statistics.hingehi; tstatistics.hingehi];
                end
            end

            retPredictions=predictions;
            datid=datElementIds{2,2};%y
            retPredictions.elements(datid).statistics=statistics;
        end

        function plotData(obj, hPlot, data)
            % Currently, only supporting linear regression
            %supports plot of original data with thresholds for predicted data (based off histograms at desired data points)
            if (strcmp(obj.type,'mcmcBayes.linear')==true)

            else
                error('Unsupported model type for mcmcBayes.regression');
            end
            if isempty(hPlot) || ~isvalid(hPlot)
                warning('No figure provided for plot.');
                return
            end
            set(0,'CurrentFigure',hPlot.hfig);
            hold on;
            datElementIds=obj.getDatElementIds();
            datid=datElementIds{1,2};
            if ~(strcmp('x',data.elements(datid).name))
                warning('linear x name not found in element(datid), datid might be incorrect');
            end            
            [x,indices]=sort(data.elements(datid).values.valueConstant.data);
            datid=datElementIds{2,2};
            if ~(strcmp('y',data.elements(datid).name))
                warning('linear y name not found in element(datid), datid might be incorrect');
            end                        
            for i=1:size(indices)
                y(i)=data.elements(datid).values.valueConstant.data(indices(i));
            end
            options='b*';
            plot(x,y,options);%plot it to hPlot.hfig
            hold off;
        end

        function plotPredictions(obj, hPlot, data, predictionStatistic)
            % Currently, only supporting linear regression
            %supports plot of original data with thresholds for predicted data (based off histograms at desired data points)
            if (strcmp(obj.type,'mcmcBayes.linear')==true)

            else
                error('Unsupported model type for mcmcBayes.regression');
            end
            if isempty(hPlot) || ~isvalid(hPlot)
                warning('No figure provided for plot.');
                return
            end
            set(0,'CurrentFigure',hPlot.hfig);
            hold on;
            datElementIds=obj.getDatElementIds();
            datid=datElementIds{1,2};
            if ~(strcmp('x',data.elements(datid).name))
                warning('linear x name not found in element(datid), datid might be incorrect');
            end            
            [x,indices]=sort(data.elements(datid).values.valueConstant.data);
            datid=datElementIds{2,2};
            if ~(strcmp('y',data.elements(datid).name))
                warning('linear y name not found in element(datid), datid might be incorrect');
            end              
            switch predictionStatistic
                case mcmcInterface.t_predictionStatistic.mean
                    for i=1:size(indices)
                        y(i)=data.elements(datid).statistics.mean(indices(i));
                    end
                    options='g-';
                case mcmcInterface.t_predictionStatistic.hingelo
                    for i=1:size(indices)
                        y(i)=data.elements(datid).statistics.hingelo(indices(i));
                    end
                    options='r-';
                case mcmcInterface.t_predictionStatistic.hingehi
                    for i=1:size(indices)                    
                        y(i)=data.elements(datid).statistics.hingehi(indices(i));
                    end
                    options='r-';
                otherwise
                    error('unsupported mcmcInterface.t_predictionStatistic ytype');                
            end
            plot(x,y,options);%plot it to hPlot.hfig
            hold off;
        end        

        function [loglikelihood]=computeLogLikelihood(obj, data, samples)
        % [loglikelihood]=computeLogLikelihood(obj, vars, data)
        %   Computes the loglikelihood of the data given the model and parameter values.  This is used in the marginal likelihood computation.
        %   Input: 
        %     obj - model object
        %     data - Independent and dependent data variables for the model
        %     samples - samples from model variables
        %   Output:
        %     loglikelihood - Computed loglikelihood (vector)          

warning('need to use getVarElementIds and getDatElementIds');
            varElementIds=obj.getVarElementIds();
            datElementIds=obj.getDatElementIds();

            x=data.elements(1).values.valueConstant.data;
            Tau=size(x,1);%Number of data (we are assuming scalar dimensions)
            yorig=data.elements(2).values.valueConstant.data;
           
            numSamples=size(samples.elements(1).values.valueGenerated.data,1);
            loglikelihood=zeros(numSamples,1);

            %need to already have called transformPost on samples             
            if strcmp(obj.type,'mcmcBayes.linear') %normal(beta0+beta1*x,1/r)%matlab uses normal(mu,sigma^2), openbugs uses normal(mu,r)
                if strcmp(obj.subtype,'b') 
                    varid=1;%subtype b (normal, normal, gamma), for beta0, varid=1
                    beta0=samples.elements(varid).values.valueGenerated.data;
                    varid=4;%subtype b (normal, normal, gamma), for beta1, varid=4
                    beta1=samples.elements(varid).values.valueGenerated.data;
                    if size(samples.elements,1)==10
                        varid=7;%subtype b (normal, normal, gamma), for r, varid=7
                        r=samples.elements(varid).values.valueGenerated.data;
                        p=2;
                        X=ones(Tau,p);
                        X(:,2)=x;
                    else
                        error('incorrect size for model %s, subtype %s, samples.elements()',obj.type,obj.subtype);
                    end
                else
                    error('unsupported subtype %s for model %s',obj.subtype,obj.type);
                end
            elseif strcmp(obj.type,'mcmcBayes.polynomialDegree2')  %normal(beta0+beta1*x+beta2*x^2,1/r)%matlab uses normal(mu,sigma^2), openbugs uses normal(mu,r)
%                 if (size(vars,1)~=4)
%                     error('incorrect size for variables');
%                 end
%                 beta2=cast(vars(3).MCMC_samples,'double');
%                 r=cast(vars(4).MCMC_samples,'double');
%                 p=3;
%                 X=ones(Tau,p);
%                 X(:,2)=x;
%                 X(:,3)=x.^2;
            else
                error('unsupported model for mcmcBayes.regression::computeLogLikelihood()');
            end
            assert(all(eig(X'*X)>0));%confirm X'*X is positive definite
            B=zeros(p,1);                

            for j=1:numSamples
                Om=diag(ones(1,Tau)*r(j));
                if strcmp(obj.type,'mcmcBayes.linear') %normal(beta0+beta1*x,1/r)%matlab uses normal(mu,sigma^2), openbugs uses normal(mu,r)
                    B(1)=beta0(j);
                    B(2)=beta1(j);
                elseif strcmp(type, 'mcmcBayes.polynomialDegree2') %normal(beta0+beta1*x+beta2*x^2,1/r)%matlab uses normal(mu,sigma^2), openbugs uses normal(mu,r)
                    B(1)=beta0(j);
                    B(2)=beta1(j);
                    B(3)=beta2(j);
                end
                mu=X*B;
                %logdetOm=2*sum(log(diag(chol(Om))));%https://xcorr.net/2008/06/11/log-determinant-of-positive-definite-matrices-in-matlab/
                %loglikelihood(j)=-Tau/2*log(2*pi)+1/2*logdetOm-1/2*(yorig-mu)'*Om*(yorig-mu);%MVN(X*B,Om) log density, where Om is the precision matrix                
                loglikelihood(j,1)=-Tau/2*log(2*pi)+Tau/2*log(r(j))-1/2*r(j)*(yorig-mu)'*(yorig-mu);%Identical computation for above when Om=(r^-1)*I, but this is more readable
            end                               
            
            if (~isempty(find(isinf(loglikelihood)==1)))
                error('Error condition in computeLogLikelihood(), loglikelihood contains -Inf or Inf');
            elseif ~isempty(find(isnan(loglikelihood)==1))
                error('Error condition in computeLogLikelihood(), loglikelihood contains Nan');
            end
        end

        function [retSamples, retData]=transformPost(obj, samples, data)
            [retSamples, retData]=transformPost@mcmcBayes.model(obj, samples, data);

            %Here is a regression model specific extension for transformPost

warning('need to use getVarElementIds and getDatElementIds');
            varElementIds=obj.getVarElementIds();
            datElementIds=obj.getDatElementIds();

            %loop through the elements in retSamples
            %retSamples.elements(sampleid).values.valueConstant.tfPost might be mcmcInterface.t_valueTransformPost.adjustForFit2MeanCenteredData
            for elemid=1:size(retSamples.elements,1)
                if retSamples.elements(elemid).distribution.type~=mcmcInterface.t_elementDistribution.fixed
                    if retSamples.elements(elemid).values.valueGenerated.tfPost==mcmcInterface.t_valueTransformPost.adjustForFit2MeanCenteredData
                        mean_x=data.elements(1).values.valueConstant.mean;
                        mean_y=data.elements(2).values.valueConstant.mean;
                        zbeta0=retSamples.elements(1).values.valueGenerated.data;
                        zbeta1=retSamples.elements(4).values.valueGenerated.data;
                        zr=retSamples.elements(7).values.valueGenerated.data;
                        retSamples.elements(1).values.valueGenerated.data=zbeta0+mean_y-zbeta1*mean_x;
                        retSamples.elements(4).values.valueGenerated.data=zbeta1;
                        retSamples.elements(7).values.valueGenerated.data=zr;
                    elseif retSamples.elements(elemid).values.valueGenerated.tfPost~=mcmcInterface.t_valueTransformPost.none
                        warning('unsupported mcmcInterface.t_valueTransformPost in transformPost');
                    end
                end
            end
        end    
    end
end

