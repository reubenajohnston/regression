classdef regressionEvaluation < mcmcBayes.study
    %REGRESSIONEVALUATION 
    %   Extends mcmcBayes.study to support running a set of sequences for demonstration of regression models on several public datasets.
    % Usage:
    % >> tmpStudy=mcmcBayes.study.loadAndConstructStudy('/opt/users/johnsra2/sandbox/mcmcbayes/models/regression/config/regressionEvaluation_study.xml');
    % >> tmpStudy.runStudy();
    
    properties
        % No custom properties
    end
    
    methods (Static)

    end

    methods
        function obj = regressionEvaluation(setUuid, setSequences)
            %REGRESSIONEVALUATION 
            %   Construct an instance of this class
            if nargin > 0
                superargs{1}='mcmcBayes.regressionEvaluation';
                superargs{2}=setUuid;
                superargs{3}=setSequences;
            elseif nargin == 0
                superargs={};
            end
            obj=obj@mcmcBayes.study(superargs{:});
        end    
    end
end

