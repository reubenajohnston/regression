%mcmcBayes example (as a study)
tmpStudy=mcmcBayes.study.loadAndConstructStudy('/opt/users/johnsra2/sandbox/mcmcbayes/models/regression/config/regressionEvaluation_study.xml'); %PosixOs
tmpStudy.sequences(1).simulations(1).clearAll()
tmpStudy.runStudy();

%It is recommended to run this via the mcmcBayes.study approach above
%However, one can optionally run this using only in the mcmcInterface (i.e., without the mcmcBayes framework)

%mcmcInterface example (as a simulation)
%tmpSimulation=mcmcInterface.simulation.loadAndConstructSimulation('/opt/users/johnsra2/sandbox/mcmcbayes/models/regression/config/linearPresetSimulatedStan_simulation.xml'); %PosixOs
%tmpSimulation.runSimulation();