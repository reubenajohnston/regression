%mcmcBayes example (as a study)
format long;
close all;
clc;
tmpStudy=mcmcBayes.study.loadAndConstructStudy('/opt/users/johnsra2/sandbox/mcmcbayes/models/regression/config/regressionEvaluation_pp_study.xml'); %PosixOs
tmpStudy.sequences(1).simulations(1).clearAll();
tmpStudy.sequences(1)=tmpStudy.sequences(1).runSequence();
tmpStudy.sequences(2)=tmpStudy.sequences(2).runSequence();
