# regression

**Warning: regression is in the pre-release stage and is still being refined**

Regression includes linear and polynomial degree-2 regression models implemented in Stan.

# Dependencies

* [mcmcBayes](https://gitlab.com/mcmcbayes/mcmcbayes.git)
* [os-agnostic-utils](https://gitlab.com/reubenajohnston/os-agnostic-utils.git)
* [mcmcInterface](https://gitlab.com/mcmcbayes/mcmcinterface.git)
* [powerPosterior](https://gitlab.com/mcmcbayes/powerposterior.git)

# Installation
1. Clone this repository into the mcmcbayes/models/regression folder as described in the mcmcBayes README.md
1. Update MATLAB path to add folders in models/regression/code/matlab)
    ```
	>> addpath /sandboxes/johnsra2/mcmcbayes/models/regression/code/matlab
	```
1. Setup config/regressionEvaluation_study.xml with appropriate paths
1. Setup config/regressionEvaluation_pp_study.xml with appropriate paths
1. Setup config/linearGenericStan_simulation.xml with appropriate paths

# Usage
* Run the main study
    ```
	>> tmpStudy=mcmcBayes.study.loadAndConstructStudy('/sandboxes/johnsra2/mcmcbayes/models/regression/config/regressionEvaluation_study.xml');
    >> tmpStudy=tmpStudy.runStudy();
	```
* Run a single sequence, e.g., sequence 1, in the study using `>> tmpStudy.sequences(1)=tmpStudy.sequences(1).runSequence();`
* Run the powerposterior study (due to lengthy simulation time, it is recommended to run the sequences individually on different Docker container environments)
    * In container 1:
    ```
    tmpStudy=mcmcBayes.study.loadAndConstructStudy('/sandboxes/johnsra2/mcmcbayes/models/regression/config/regressionEvaluation_pp_study.xml');
    >> tmpStudy.sequences(1)=tmpStudy.sequences(1).runSequence();
	```
    * In container 2:
    ```
    tmpStudy=mcmcBayes.study.loadAndConstructStudy('/sandboxes/johnsra2/mcmcbayes/models/regression/config/regressionEvaluation_pp_study.xml');
    >> tmpStudy.sequences(2)=tmpStudy.sequences(2).runSequence();
	```